import {
  GraphQLError,
  GraphQLNonNull,
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';
import jwt from 'jsonwebtoken';
import { users } from '../../mockup';

export default {
  args: {
    email: {
      description: 'User email',
      type: new GraphQLNonNull(GraphQLString),
    },
    password: {
      description: 'User password',
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  description: 'Mutation to perform a login',
  resolve: (parent, args) => {
    const { email, password } = args;
    const findUser = users.find(user => user.email === email && user.password === password);

    if (!findUser) {
      throw new GraphQLError('Invalid credentials');
    }

    const token = jwt.sign({ email }, 'secret', {
      expiresIn: '90d',
    });

    return { token };
  },
  type: new GraphQLNonNull(new GraphQLObjectType({
    description: 'JWT Token',
    fields: () => ({
      token: {
        description: 'Token',
        resolve: source => source.token,
        type: new GraphQLNonNull(GraphQLString),
      },
    }),
    name: 'GraphQLTokenObject',
  })),
};
