import { GraphQLObjectType } from 'graphql';
import Login from './Login';

export default new GraphQLObjectType({
  fields: () => ({
    Login,
  }),
  name: 'RootMutationType',
});
