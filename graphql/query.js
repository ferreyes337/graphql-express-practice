import {
  GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLList,
} from 'graphql';

import { users, posts } from '../mockup';
import { GraphQLUserType, GraphQLPostType } from './types';

export const query = new GraphQLObjectType({
  fields: {
    User: {
      args: {
        email: {
          description: 'Users email',
          type: GraphQLNonNull(GraphQLString),
        },
      },
      description: 'Query to fetch specific user',
      resolve: (parent, args) => {
        const { email } = args;

        const findUsers = users.find(user => user.email === email);

        return findUsers;
      },
      type: GraphQLUserType,
    },
    Posts: {
      description: 'Query to fetch specific user',
      resolve: () => posts,
      type: new GraphQLNonNull(new GraphQLList(GraphQLPostType)),
    },
    Post: {
      args: {
        key: {
          description: 'Post key',
          type: new GraphQLNonNull(GraphQLString),
        },
      },
      description: 'Query to fetch a post with the key',
      resolve: (parent, args) => {
        const { key } = args;
        const findPost = posts.find(post => post.key === key);
        return findPost;
      },
      type: GraphQLPostType,
    },
  },
  name: 'RootQueryType',
});
