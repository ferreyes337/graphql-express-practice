import { GraphQLObjectType, GraphQLNonNull, GraphQLString } from 'graphql';
import { users } from '../../mockup';
import { GraphQLUserType } from './UserType';

export const GraphQLPostType = new GraphQLObjectType({
  description: 'Post definition type',
  fields: () => ({
    author: {
      description: 'Post author',
      resolve: (parent) => {
        const { author } = parent;
        const findAuhor = users.find(user => user.id === author);
        return findAuhor;
      },
      type: new GraphQLNonNull(GraphQLUserType),
    },
    content: {
      description: 'Post content',
      resolve: (parent) => {
        const { content } = parent;
        return content;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    coverPicture: {
      description: 'Post cover picture',
      resolve: (parent) => {
        const { coverPicture } = parent;
        return coverPicture;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    title: {
      description: 'Post title',
      resolve: (parent) => {
        const { title } = parent;
        return title;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    key: {
      description: 'Post title',
      resolve: (parent) => {
        const { key } = parent;
        return key;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
  name: 'GraphQLPostType',
});
