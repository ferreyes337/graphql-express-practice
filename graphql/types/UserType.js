import {
  GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLList,
} from 'graphql';
import { GraphQLPostType } from './PostType';
import { posts } from '../../mockup';

export const GraphQLUserType = new GraphQLObjectType({
  desciption: 'User definition type',
  fields: () => ({
    name: {
      description: 'User name',
      resolve: (parent) => {
        const { name } = parent;
        return name;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      description: 'User email',
      resolve: (parent) => {
        const { email } = parent;
        return email;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    bio: {
      description: 'User bio',
      resolve: (parent) => {
        const { bio } = parent;
        return bio;
      },
      type: new GraphQLNonNull(GraphQLString),
    },
    posts: {
      description: 'User posts',
      resolve: (parent) => {
        const { id } = parent;
        const userPosts = posts.filter(post => post.author === id);
        return userPosts;
      },
      type: new GraphQLNonNull(new GraphQLList(GraphQLPostType)),
    },
  }),
  name: 'GraphQLUserType',
});
