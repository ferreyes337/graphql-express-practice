export const users = [
  {
    id: 1,
    name: 'Fernando',
    email: 'fernando@mail.com',
    password: 'secret',
    bio: 'Love life',
    posts: [],
  },
  {
    id: 2,
    name: 'Joel',
    email: 'joel@mail.com',
    password: 'secret',
    bio: 'Hate u',
    posts: [],
  },
  {
    id: 3,
    name: 'Oscar',
    email: 'oscar@mail.com',
    password: 'secret',
    bio: 'Ahh!',
    posts: [],
  },
];

export const posts = [
    {
      title: 'Post 2',
      content: 'lorem ipsum dolor sit amet',
      coverPicture: 'http://mycoverpoint.com/wp-content/uploads/Lorem-Ipsum-Facebook-Timeline-Cover.png',
      author: 1,
      key: 'DFGDH',
    },
    {
      title: 'Post 1',
      content: 'lorem ipsum dolor sit amet',
      coverPicture: 'http://mycoverpoint.com/wp-content/uploads/Lorem-Ipsum-Facebook-Timeline-Cover.png',
      author: 3,
      key: 'ETWEW',
    },
    {
      title: 'Post 3',
      content: 'lorem ipsum dolor sit amet',
      coverPicture: 'http://mycoverpoint.com/wp-content/uploads/Lorem-Ipsum-Facebook-Timeline-Cover.png',
      author: 1,
      key: 'FGJFS',
    },
    {
      title: 'Post 4',
      content: 'lorem ipsum dolor sit amet',
      coverPicture: 'http://mycoverpoint.com/wp-content/uploads/Lorem-Ipsum-Facebook-Timeline-Cover.png',
      author: 2,
      key: 'XCVXF',
    },
  ];
