import express from 'express';
import expressGraphql from 'express-graphql';
import { schema } from '../graphql';

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

router.use('/graphql', expressGraphql({
  graphiql: true,
  schema,
}));

export default router;
